package main

import (
	"math/rand"
)

func BinarySearch(hi int, leEqFn func(int) (bool, bool)) (int, bool) {
	var lo, mi int
	for lo < hi {
		mi = (lo + hi) >> 1
		le, eq := leEqFn(mi)
		if eq {
			return mi, true
		} else if le {
			lo = mi + 1
		} else {
			hi = mi
		}
	}
	return lo, false
}
func GenerateRandomWords(n, nChr int) *string {
	bs := make([]byte, n*nChr+n-1)
	j := nChr
	for i := 0; i < len(bs); i++ {
		if i != j {
			bs[i] = 'A' + byte(rand.Intn(26))
		} else {
			bs[i] = ' '
			j += nChr + 1
		}
	}
	dup := make([]int, n)
	for i := 0; i < n; i++ {
		dup[i] = rand.Intn(n)
	}
	toggle := true
	for i := 1; i < n; i++ {
		if dup[i] < i {
			j := dup[i]*nChr + dup[i]
			k := i*nChr + i
			copy(bs[k:], bs[j:j+nChr])
			for l := k; l < (k + nChr); l++ {
				if toggle {
					bs[l] |= 0b00100000
				}
				toggle = !toggle
			}
		}
	}
	str := string(bs)
	return &str
}
func NumWordsInStr(pStr *string) (n int) {
	strLen := len(*pStr)
	bgn := 0
	for bgn < strLen {
		for bgn < strLen && (*pStr)[bgn] == ' ' {
			bgn++
		}
		end := bgn
		for end < strLen && (*pStr)[end] != ' ' {
			end++
		}
		if bgn != end {
			n++
		}
		bgn = end + 1
	}
	return
}
func CaseInsensitiveLeEq(str1 *string, bgn1, end1 int, str2 *string, bgn2, end2 int) (bool, bool) {
	for bgn1 < end1 && bgn2 < end2 && ((*str1)[bgn1]|0b00100000) == ((*str2)[bgn2]|0b00100000) {
		bgn1++
		bgn2++
	}
	if bgn1 != end1 && bgn2 != end2 {
		return ((*str1)[bgn1] | 0b00100000) < ((*str2)[bgn2] | 0b00100000), false
	} else if bgn1 == end1 && bgn2 == end2 {
		return false, true
	} else {
		return bgn2 < end2, false
	}
}

type Word struct {
	PStr            *string
	Bgn, End, Count int
}

func WordsCount(words []Word, num int, pStr *string) []Word {
	if *pStr == "" {
		return nil
	}
	strLen := len(*pStr)
	bgn := 0
	if num == 0 {
		for bgn < strLen && (*pStr)[bgn] == ' ' {
			bgn++
		}
		end := bgn
		for end < strLen && (*pStr)[end] != ' ' {
			end++
		}
		if bgn != end {
			words[0].Bgn = bgn
			words[0].PStr = pStr
			words[0].Count = 1
			words[0].End = end
			num = 1
		}
		bgn = end + 1
	}
	for bgn < strLen {
		for bgn < strLen && (*pStr)[bgn] == ' ' {
			bgn++
		}
		end := bgn
		for end < strLen && (*pStr)[end] != ' ' {
			end++
		}
		if bgn != end {
			idx, eq := BinarySearch(num, func(mi int) (bool, bool) {
				return CaseInsensitiveLeEq(words[mi].PStr, words[mi].Bgn, words[mi].End, pStr, bgn, end)
			})
			if eq {
				words[idx].Count++
			} else {
				if idx != num {
					copy(words[idx+1:], words[idx:num])
				}
				words[idx].PStr = pStr
				words[idx].Count = 1
				words[idx].Bgn = bgn
				words[idx].End = end
				num++
			}
			bgn = end + 1
		}
	}
	return words[0:num]
}
func WordsAlloc(words []Word, sz int) (ret []Word) {
	ret = make([]Word, len(words)+sz)
	copy(ret, words)
	return
}
func PrintWordsCount(words []Word) {
	for i := 0; i < len(words); i++ {
		print((*words[i].PStr)[words[i].Bgn:words[i].End], ":", words[i].Count, " ")
	}
	println()
}
func main() {
	var words []Word
	for i := 0; i < 3; i++ {
		pStr := GenerateRandomWords(8, 5)
		println(*pStr)
		prevAllocSz := len(words)
		words = WordsCount(WordsAlloc(words, NumWordsInStr(pStr)), prevAllocSz, pStr)
		PrintWordsCount(words)
	}
}
